*************************************************************************
*                                                                       *
*                      SeePy 0.6 (beta version)                         *
*         Easy and fast dynamic report generation with Python           *
*                                                                       *
*                     (c) 2016-2018 Lukasz Laba                         *
*                                                                       *
*************************************************************************

SeePy is a tool to create interactive report for scientific calculation 
you made with python script.
The mission of the project is to provide a simple and practical tool that 
will interest engineers to use Python language in their daily work.

SeePy is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

SeePy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

-------------------------------------------------------------------------
* SeePy creator:
-------------------------------------------------------------------------
  Lukasz Laba, e-mail : lukaszlab@o2.pl

-------------------------------------------------------------------------
* SeePy contributors:
-------------------------------------------------------------------------
  Special thanks to those who have made contributions:
    
  Albert Defler (2016)
      - code refactoring, testing, helpful advices.

-------------------------------------------------------------------------
* Visible changes:
-------------------------------------------------------------------------
  SeePy 0.6.1 (6th beta)
  - dxf file graphics displaying
  SeePy 0.5.1 (5th beta)
  - saveas upgrade
  - open read only option added
  - seepy temporary file as python tempfile
  - seepy timer feature added
  - pillow/PIL PIL.Image instance display available
  - filepath and dirpath dialog available for interaction
  - True/Fales value checkbox for interaction
  SeePy 0.4.3 (4rd beta)
  - system command line inerface available
  - utf-8 display 
  - source code refactored
  - program stability and user interface improved
  SeePy 0.3.2 (3rd beta corected)
  - one small bug fixed  
  SeePy 0.3.1 (3rd beta)
  - watch script option added
  - save and saveas available
  - import from script directory available
  - some new syntax availavle in SeePy script
  - code indentation tested  
  SeePy 0.2.2 (2nd beta)
  - 2nd beta version
  - some new syntax availavle in SeePy script
  - all UI moved to PyQt4  
  SeePy 0.2.1 (1st beta)
  - main features implemented (for testing)  
  SeePy 0.1.2 (alpha)
  - alpha version (for testing)  
  SeePy 0.0.2 (pre-alpha)
  - no changes  
  SeePy 0.0.1 (pre-alpha)
  - first PyPI version

-------------------------------------------------------------------------
* Prerequisites:
-------------------------------------------------------------------------
  Python 2.7.
  Non-standard Python library needed:
  - PyQt4
  - mistune
  - matplotlib (not necessary - needed for working with matplotlib and LaTex)
  - svgwrite (not necessary - needed for working with SVG graphic)
  - pillow (not necessary - needed for working with image display  and image 
    procesing)
  - dxf2svg (not necessary - needed for dxf file displaying)

-------------------------------------------------------------------------
* To install SeePy:
-------------------------------------------------------------------------
  After the Python and needed library  was installed:
  On Windows use "pip install seepy" in Python shell.

  To run SeePy GUI execute SeePy.py from installed SeePy
  package folder - probabiliit is "C:\Python27\Lib\site-packages\seepy"
  For easy run make shortcut on your system pulpit to this file.
  There is install instruction on project website.
  https://bitbucket.org/lukaszlaba/seepy/wiki/installing
  Windows (7) and Linux (xubuntu, fedora) tested.
   
-------------------------------------------------------------------------
* Other information :
-------------------------------------------------------------------------
  - Project website: 
    https://bitbucket.org/lukaszlaba/seepy
  - E-mail : lukaszlab@o2.pl

-------------------------------------------------------------------------