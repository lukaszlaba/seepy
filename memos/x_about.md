![Alt text](x_monty.png)

|Use See icon to back to your script|


#*SeePy 0.6 (6th beta version)*

Python script visualization tool

(c) 2016-2018 Łukasz Laba

##*License*

SeePy is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

SeePy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

##*Other information*
Project website: 
https://bitbucket.org/lukaszlaba/seepy

E-mail : lukaszlab@o2.pl